from django.apps import AppConfig


class UpdatingListConfig(AppConfig):
    name = 'updating_list'
