from django.conf.urls import url
from updating_list import views

app_name = "updating_list"

# urls down below

urlpatterns = [
    url(r'^todo_list/$', views.todo_list, name="todo_list"),
    url(r'^contact/$', views.contact, name="contact"),
    url(r'^about/$', views.about, name="about")
]
