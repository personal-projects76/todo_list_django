from django.shortcuts import render

# Create your views for updating_list here.


def index(request):
    return render(request, 'index.html')

def todo_list(request):
    return render(request, 'updating_list/todo_list.html')

def contact(request):
    return render(request, 'updating_list/contact_me.html')

def about(request):
    return render(request, 'updating_list/about_me.html')