from django.conf.urls import url
from user_info import views

app_name = "user_info"

# urls for user_info down below

urlpatterns = [
    url(r'^login/$', views.login, name="login"),
    url(r'^registration/$', views.registration, name="registration")
]
