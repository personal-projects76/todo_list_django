from django.shortcuts import render

# Create your views for user_info here.

def login(request):
    return render(request, 'user_info/login.html')

def registration(request):
    return render(request, 'user_info/registration.html')